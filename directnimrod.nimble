[Package]
name = "directnimrod"
version = "0.2.0"
author = "Charlie Barto"
description = """Nimrod wrapper for the
               direct3D 11, 11.1, and 12 libraries
               as well as for direct2D and DXGI"""
license = "MS-PL"

srcDir = "src"
skipDirs = "wrap"
